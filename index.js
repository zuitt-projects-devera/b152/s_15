console.log("HELLO WORLD");
document.getElementById("js").innerHTML = "JS Selection Control";


let numString1 = 5;
let numString2 = 10;
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;



function subtract(num1, num2) {
	
	if (num1 < num2) {
		console.log("The first number should be greater than the second number.");
	}
	else {
		return num1 - num2;
	}
}

let difference = subtract(2, 2);
console.log(difference);


function multiply(num1, num2) {
	

		return num1 * num2;
	
}

let product = multiply(5, 2);
console.log(product);



function divide(num1, num2) {
	
	if (num1 < num2) {
		console.log("The first number should be greater than the second number.");
	}
	else {
		return num1 / num2;
	}
}

let quotient = divide(50, 5);
console.log(quotient);
console.log(product*0);
console.log(quotient/0);






let sum = 10;
sum += 20;
console.log(sum);

/*let sum2 = 5;
sum2 += 5;
console.log(sum2);
console.log(5+=5);*/


let sum3 = 5;
sum3 += "cry";
console.log(sum3);




//Order of Operations follow (MDAS)

let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);
console.log("We are \"Vikings\"");

let txt = "Hello world";
txt = txt.toUpperCase();
console.log(txt);





console.log(1 == 1);
console.log(1 == '1'); /*true - loose equality operator prioritizes the sameness of the 
value because Loose Equality Operator actually enforces what we call Forced Coercion or 
when the types of the operands are automatically changed before the comparison. The string
here was actually converted to number. 1 == 1 = true.*/

console.log (1 == true); /*true - with forced coercion, since the operands have different
types, both were converted number, 0 is already a number but false is a boolean, the boolean
false when converted to number is 0. And thus, 0 == 0 = true*/

/*JS has a function/method which allow us to convert data from one type to number */

let sampleConvert = Number(false);
console.log(sampleConvert);

let sampleConvert2 = "2500";
sampleConvert2 = Number(sampleConvert2);
console.log(sampleConvert2);

console.log(true == "true");

//Strict Equality Operator

/*Strict Equality operator evaluates the sameness of bot values and types of operands.
Thus, Strict Equality Operator is more preferred because JS is a loose-typed language.*/

console.log(1 == "1"); //In loose equality (==) this is true.
console.log(1 === "1"); //false - checks sameness of both values and types of operands.
console.log("james2000" === "James2000"); //false = j not equal to J
console.log(55 === "55"); // false - strict equality operator (===) checks sameness of value and type

//Loose Inequality Operator
	//Checks whether the operands are NOT equal and or/have different values.
//Much like Loose Equality Operator, Loose Inequality Operator also does forced coercion


console.log(1 != "1");

//Equality Operators and Inequality Operators with Variables:

let nameStr1 = "Juan";
let nameStr2 = "Jack";
let numberSample = 50;
let numberSample2 = 60;
let numStr1 = "15";
let numStr2 = "25";

console.log(numStr1 == 50);
console.log(numStr1 == 15);
console.log(numStr2 === 25);
console.log(nameStr2 != "James");
console.log(numberSample !== "50");
console.log(numberSample != "50");
console.log(nameStr1 == nameStr2);
console.log(nameStr2 === "jack");



/*Comparison Operator which will check the relationship between operands and 
return boolean*/

let price1 = 500;
let price2 = 700;
let price3 = 8000;
let numStrSample = "5500";

//greater than (>)
console.log(price1 > price2);
console.log(price3 > price2);
console.log(price3 > numStrSample);


//Logical Operators
//and &&
/*And operator evaluates both the left and right operands and both let and right operands
must be true so that the operation would result to true. If at leats one operand is false
then, the operation would result to false.*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log (isRegistered && isLegalAge);
console.log (isAdmin && isRegistered);

let user1 = {
	username: "peterphoenix_1999",
	age: 28,
	level: 15,
	isGuildAdmin: false
}

let user2 = {
	username: "kingBrodie00",
	age: 13,
	level: 50,
	isGuildAdmin: true
}
let user3 = {
	username: "aaaa",
	age: 21,
	level: 20,
	isGuildAdmin: true
}

/*to be able to access the properties of an object, we use the dot (.) notation: */
console.log(user1.username);
console.log(user1.level);

/*auth sample*/
let authorization1 = user1.age >= 18 && user1.level >= 25;
console.log(authorization1);


let authorization2 = user2.age >= 18 && user2.level >= 25;
console.log(authorization2);

//Guild Leaders Meeting
let authorization3 = user1.level >= 10 && user1.isGuildAdmin;
console.log(authorization3);

let authorization4 = user2.level >= 10 && user2.isGuildAdmin;
console.log(authorization4);

//Or ||
/* Or operator returns true if at least one operand results to true.*/

/*
let user1 = {
	username: "peterphoenix_1999",
	age: 28,
	level: 15,
	isGuildAdmin: false
}

let user2 = {
	username: "kingBrodie00",
	age: 13,
	level: 50,
	isGuildAdmin: true
}
*/
//On site meeting between new users
let authorization5 = user1.age >= 18 || user1.level <= 15; 
console.log(authorization5);




/*IF ELSE */

/*if (user1.age >= 18) {
	alert("You are allowed to enter!");
}

if (user2.age >= 18) {
	alert("user2 you are allowed to enter!");
}
else {
	alert("User2, you are not allowed to enter.");
}*/




if (user1.level >= 20) {
	console.log("User1 is not a noobie");
}
else {
	console.log("User1 is a noobie");
}

if (user2.level >= 20) {
	console.log("User2 is not a noobie");
}
else {
	console.log("User2 is a noobie");
}

/*miniactivity*/

if (user1.isGuildAdmin) {
console.log("Welcome Back, Guild Admin");

}

else {
console.log("You are not authorized to enter.");

}


if (user2.level >= 35) {
	console.log("Hello, Knight!");
} 
else if (user2.level >= 25) {
	console.log("Hello, Swordsman!");
}
else if (user2.level >= 10) {
	console.log("Hello, rookie!");
}
else {
	console.log("Level out of range.");
}

//Logical Operators for If conditions:

let usernameInput1 = "hale";
let passwordInput1 = "lorem ipsum"

let usernameInput2 = ""
let passwordInput2 = null;



function login() {
	let username = prompt("Type in your username.");
	let pw = prompt("Type in your password.");

	

if (usernameInput1 === "" || passwordInput1 === "") {
	console.log("Complete the forms.");
}
else {
if (usernameInput1 === username && passwordInput1 === pw) {
	console.log("Welcome, user1!");
}
else {
	console.log("Invalid username or password.");
}  
}
}
/*
let user1 = {
	username: "peterphoenix_1999",
	age: 28,
	level: 15,
	isGuildAdmin: false
}

let user2 = {
	username: "kingBrodie00",
	age: 13,
	level: 50,
	isGuildAdmin: true
}

let user3 = {
	username: "aaaa",
	age: 21,
	level: 20,
	isGuildAdmin: true
}

*/


function requirementChecker(level, isGuildAdmin) {
	if(level <= 25 && isGuildAdmin === false) {
		console.log("Welcome to the Newbies Guild");
	}
	else if (level > 25){
		console.log("You are too stronk.");
	}
	else if (isGuildAdmin === true){
		console.log("You are a guild admin");
	}

}
requirementChecker(user2.level, user2.isGuildAdmin);



function addNum (num1, num2) {
	//check if the numbers being passed are both number types.
	//typeof keyword returns a string which tells the type of data that follows it.

	if (typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only if both arguments passed are number type.");
	}
	else { 
		console.log("One or both of the arguments are not numbers. .");
	}

}

let str = "sample";

console.log(typeof str);

function dayChecker(colorcoding) {
let day = prompt("Today is: ");

colorcoding = day;
console.log(colorcoding);
switch (colorcoding.toLowerCase()) {
case "sunday":
	console.log("Today is " + colorcoding + "; Wear White");
	break;
	case "monday":
	console.log("Today is " + colorcoding + "; Wear Blue");
	break;
	case "tueday":
	console.log("Today is " + colorcoding + "; Wear Green");
	break;
	case "wednesday":
	console.log("Today is " + colorcoding + "; Wear Purple");
	break;
	case "thursday":
	console.log("Today is " + colorcoding + "; Wear Brown");
	break;
	case "friday":
	console.log("Today is " + colorcoding + "; Wear Red");
	break;
	case "saturday":
	console.log("Today is " + colorcoding + "; Wear Pink");
	break;
	default:
	console.log("Invalid input. There is no such day as " + colorcoding);
}
}

function capitalChecker(country) {
	switch(country) {
		case "philippines":
			console.log("Manila");
			break;
		case "usa":
			console.log("Washington D.C.");
			break;
		case "japan":
			console.log("Tokyo");
			break;
		case "germany":
			console.log("Berlin");
			default:
			console.log("Input is out of range. Choose another country.");
		}

}


capitalChecker("usa");





















function findStrongestChar(char) {
	

	char = document.getElementById("power").value;
	switch (char) {
		case "Eugene":
		console.log("Your power level is 20000");
		break;
		case "Dennis":
		console.log("Your power level is 15000");
		break;
		case "Vincent":
		console.log("Your power level is 14500");
		break;
		case "Jeremiah":
		console.log("Your power level is 10000");
		break;
		case "Alfred":
		console.log("Your power level is 8000");
		break;
		default:
		console.log("No characters named: " + char);
	}
}

/* Ternary Operator
		Conditionbal statement as if-else. However it was introduced to be short hand way
		to write if-else.

*/
	let superHero = "Batman"
	superHero === "Batman" ? console.log ("You are rich.") :
	console.log ("Bruce Wayne is richer than you.");

	/*superHero === "Superman" ? console.log("Hi, Clark!");*/


	let currentRobin = "Tim Drake";
	let isFirstRobin = currentRobin === "Dick Grayson" ? true : false;
	console.log(isFirstRobin);





/*Switch is a conditional statement which be an alternative to else-if, if-else, 
where the data being checked or evaluated is of an expected input

Switch will compare your expression/condition to match with a case. Then the statement
for that case will run.

	syntax:

	switch (expression/condition)
	case: value:
	statement;
	break;
	case: value2:
	statement;
	break
	default:
	statement;
	break;
	*/


/*if (day === "Sunday") {
	
	console.log("Today is Sunday; Wear White");
}
else if (day === "Monday") {

console.log("Today is Monday; Wear Blue");
}
else if (day === "Tuesday") {
console.log("Today is Tuesday; Wear Green");
}
else if (day === "Wednesday") {
console.log("Today is Wednesday; Wear Purple");
}
else if (day === "Thursday") {
console.log("Today is Thursday; Wear Brown");
}
else if (day === "Friday") {
console.log("Today is Friday; Wear Red");
}
else if (day === "Saturday") {
console.log("Today is Saturday; Wear Pink");
}
else {
console.log("There's no such day as " +day);
}

console.log(colorcoding);*/




























/*addNum(2, 5)
addNum("2", 2)*/
/*
function tryThis() {
	for (var i = 0; i >= 10; i++) {

		console.log(numString1++);
	}

	
}


tryThis();*/